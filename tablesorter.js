/*
The MIT License (MIT)
Copyright © 2013 Florian Schwehn

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

(function($){

var _t0;

function resetTime() {
  _t0 = new Date().valueOf();
}
function logTime(label) {
  var _t1 = new Date().valueOf();
  var elapsed = _t1 - _t0;
  console.log('time elapsed[' + label + ']: ' + elapsed);
  _t0 = _t1;
}
resetTime();

// table dorter constructor
function TableSorter(table, options){
  this.table = table;
  this.options = $.extend({
      initialSort: {
        column: 0,
        ascending: true
      },
      columns: {},
      cache: true
  }, options);
  this.init();
};

// table dorter prototype
TableSorter.prototype = {
  init: function(){
    var self = this;
    var $ths = $('thead th.sort', this.table);
    var colCount = $ths.size();

    $(this.table).addClass('sort');

    $ths.addClass('none')
        .css('white-space', 'nowrap')
        .append('&nbsp;<span class="icon"></span>')

    // click functions
    $ths.click(function(){
      var index = $('thead th', this.table).index(this);
      var asc = !$(this).hasClass('ascending');
      self.sort(index, asc);
    });

    // cache sort values
    window.setTimeout(function(){
      if (self.options.cache)
        self.cacheSortValues();
      
      // initial sorting
      if (self.options.initialSort) {
        var asc = self.options.initialSort.ascending;
        asc = (typeof asc == 'undefined') ? true : !!asc;
        self.sort(self.options.initialSort.column, asc);
      }
    }, 0);
  },

  cacheSortValues: function() {
    var self = this;
    var rowIndex = 0;
    $('thead th').each(function(){
      var $th = $(this);
      if ($th.hasClass('sort')) {
        // get sort value function
        var colOpts = self.options.columns[rowIndex] || {};
        var getSortValue = colOpts.sortValue;
        
        if (typeof getSortValue != 'function') {
          getSortValue = function($cell) {
            return $cell.text();
          }
        }

        $('tbody tr', self.table).each(function(){
          var cell = this.children[rowIndex];
          cell.sortValue = getSortValue($(cell));
        })
      }
      rowIndex++;
    });
  },

  sort: function(index, asc){
    // resetTime();
    var colOpts = this.options.columns[index] || {};
    var $th = $($('thead th', this.table)[index]);
    
    $('thead th.sort', this.table).removeClass('ascending descending none');
    $th.siblings('th.sort').addClass('none');

    if (asc) {
      $th.addClass('ascending');
    }
    else {
      $th.addClass('descending');
    }

    var rows = $('tbody tr', this.table);
    var tmp = [];

    rows
      .detach()
      .each(function(){
        tmp.push(this);
      })
    ;

    // get compare function
    var compare = colOpts.sort;
    switch (typeof compare) {
      case 'function':
        break;
      case 'undefined':
        compare = this.compareFunctions.text;
        break;
      default:
        compare = this.compareFunctions[compare];
        break;
    }
    // logTime('start sort');
    if (this.options.cache) {
      // sort rows
      sorted = tmp.sort(function(a, b){
        a = a.children[index].sortValue;
        b = b.children[index].sortValue;
        var cmp = compare(a, b);
        return asc ? cmp : -cmp;
      });
    }
    else {
      // get sort value function
      var getSortValue = colOpts.sortValue;
      if (typeof getSortValue != 'function') {
        getSortValue = function($cell) {
          return $cell.text();
        }
      }

      // sort rows
      sorted = tmp.sort(function(a, b){
        a = getSortValue($(a.children[index]));
        b = getSortValue($(b.children[index]));
        var cmp = compare(a, b);
        return asc ? cmp : -cmp;
      });
    }
    // logTime('end sort');

    // append sorted rows
    var tbody = $('tbody', this.table)[0];
    for (var i in sorted) {
      tbody.appendChild(sorted[i]);
    }
    // window.setTimeout(function(){ logTime('finished'); },0);
  },
  compareFunctions: {
    text: function(a, b) {
      return a < b ? -1 : 1;
    },
    number: function(a, b) {
      return a - b;
    },
    time: function(a, b) {
      a = a.split(':');
      b = b.split(':');
      for (var i = 0; i < a.length && i < b.length; i++) {
        r = a[0] - b[0];
        if (r != 0) break;
      }
      return r;
    }
  }
};

// table sorter jQuery plugin
$.fn.extend({
  tablesorter: function(arg0){
    var tableSorterID = 'jQuery_TableSorterInstance';
    
    switch (typeof arg0) {
      // constructor
      case 'object': {
        return this.each(function(){
          this[tableSorterID] = new TableSorter(this, arg0);
        });

        break;
      }
      // method calls
      case 'string': {
        break;
      }
    }
  }
});

})(jQuery);
